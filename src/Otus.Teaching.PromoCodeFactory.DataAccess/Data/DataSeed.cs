﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class DataSeed
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            #region Roles
            var admin = new Role()
            {
                Id = Guid.Parse("f0ed3ecc-42ac-4c2f-bdd1-a5e20ca2775a"),
                Name = "admin",
                Description = "Админ",
            };

            var partnerManager = new Role()
            {
                Id = Guid.Parse("e979a640-3234-4228-ba52-671a628a7dfe"),
                Name = "partnerManager",
                Description = "Партнерский менеджер"
            };

            modelBuilder.Entity<Role>().HasData(admin, partnerManager);
            #endregion

            #region Employee
            var employeeAdmin = new Employee()
            {
                Id = Guid.Parse("00db750a-eca5-4cc4-8f44-2897a6458b3d"),
                Email = "admin@company.ru",
                FirstName = "Андрей",
                LastName = "Андреев",
                RoleId = admin.Id
            };
            var employeePartner = new Employee()
            {
                Id = Guid.Parse("32a1f87a-76a2-46e6-976a-73e0d46d1286"),
                Email = "manager@company.ru",
                FirstName = "Дмитрий",
                LastName = "Дмитриев",
                RoleId = partnerManager.Id
            };

            modelBuilder.Entity<Employee>().HasData(employeeAdmin, employeePartner);
            #endregion

            #region Preference
            var rail = new Preference()
            {
                Id = Guid.Parse("c20fff29-3aef-4897-ba52-f8dd4375193e"),
                Name = "Жд",
            };
            var fly = new Preference()
            {
                Id = Guid.Parse("5fc466dc-8850-4836-af10-1f6b284981a9"),
                Name = "Авиа",
            };
            var water = new Preference()
            {
                Id = Guid.Parse("19854661-22e1-47b2-8776-2f034f449ce1"),
                Name = "Вода",
            };

            modelBuilder.Entity<Preference>().HasData(rail, fly, water);
            #endregion

            #region PromoCode
            var promo = new PromoCode
            {
                Id = Guid.Parse("b70a8cb9-af2e-41c7-a74b-c4e9cb70d0b6"),
                Code = "promo",
                BeginDate = DateTime.UtcNow,
                EndDate = DateTime.UtcNow.AddDays(30),
                PartnerId = employeePartner.Id,
                PartnerName = employeePartner.FullName,
                PreferenceId = rail.Id,
                ServiceInfo = "seeds promoCode"
            };

            modelBuilder.Entity<PromoCode>().HasData(promo);
            #endregion

            #region Customer
                var customer = new Customer
                {
                    Id = Guid.Parse("af296c19-ac17-41b7-8e6d-b937407c1f8b"),
                    FirstName = "Петр",
                    LastName = "Петров",
                    Email = "client@other.ru"
                };

                modelBuilder.Entity<Customer>().HasData(customer);
            #endregion

            #region CustomerPreference
                modelBuilder.Entity<CustomerPreference>().HasData(new CustomerPreference
                {
                    CustomerId = customer.Id,
                    PreferenceId = rail.Id,
                });
            #endregion

            #region CustomerPromoCode
                modelBuilder.Entity<CustomerPromoCode>().HasData(new CustomerPromoCode
                {
                    CustomerId = customer.Id,
                    PromoCodeId = promo.Id,
                });
            #endregion
        }
    }
}