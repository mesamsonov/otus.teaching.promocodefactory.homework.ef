﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configuration
{
    public class CustomerPromoCodeConfiguration : IEntityTypeConfiguration<CustomerPromoCode>
    {
        public void Configure(EntityTypeBuilder<CustomerPromoCode> builder)
        {
            builder.HasKey(x => new { x.PromoCodeId, x.CustomerId });

            builder.HasOne(x => x.PromoCode)
                .WithOne(x => x.CustomerPromoCode);

            builder.HasOne(x => x.Customer)
                .WithMany(x => x.CustomerPromoCodes)
                .HasForeignKey(x => x.CustomerId);
        }
    }
}