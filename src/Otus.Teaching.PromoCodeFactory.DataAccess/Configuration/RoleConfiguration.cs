﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configuration
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasIndex(x => x.Name).IsUnique();
            
            builder.Property(x=>x.Name)
                .IsRequired()
                .HasMaxLength(255);

            builder.HasMany<Employee>(x=>x.Employees)
                .WithOne(x => x.Role)
                .HasForeignKey(x => x.RoleId);
        }
    }
}