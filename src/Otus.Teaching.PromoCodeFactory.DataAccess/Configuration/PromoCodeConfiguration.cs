﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Configuration
{
    public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> builder)
        {
            builder.HasKey(e => e.Id);

            builder.HasIndex(e => e.Code).IsUnique();

            builder.Property(x => x.Code)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(x => x.BeginDate)
                .IsRequired();

            builder.Property(x => x.EndDate)
                .IsRequired();

            builder.Property(x => x.ServiceInfo)
                .IsRequired()
                .HasMaxLength(255);

            builder.Property(x => x.PartnerName)
                .IsRequired()
                .HasMaxLength(255);

            builder.HasOne(x => x.PartnerManager)
                .WithMany(x => x.PromoCodes)
                .HasForeignKey(x => x.PartnerId);

            builder.HasOne(x => x.Preference)
                .WithMany(x => x.PromoCodes)
                .HasForeignKey(x => x.PreferenceId);
        }
    }
}