﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DatabaseContext _context;

        public EfRepository(DatabaseContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var records = await _context.DbSet<T>().ToListAsync();
            return records;
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            var entity = GetIfExist(id);
            return entity;
        }

        public async Task CreateAsync(T entity)
        {
            await _context.DbSet<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            await GetIfExist(entity.Id);
            _context.DbSet<T>().Update(entity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await GetIfExist(id);
            _context.DbSet<T>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        private async Task<T> GetIfExist(Guid id)
        {
            var entity = await _context.DbSet<T>().FindAsync(id);
            if (entity == null)
                throw new Exception("Entity by id not found");
            return entity;
        }
    }
}