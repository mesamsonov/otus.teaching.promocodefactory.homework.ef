﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper
{
    public static class PromoCodeMapper
    {
        public static PromoCodeShortResponse ShortFromDto(this PromoCode promoCode)
        {
            return new PromoCodeShortResponse
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate = promoCode.BeginDate.ToString(),
                EndDate = promoCode.EndDate.ToString(),
                PartnerName = promoCode.PartnerName
            };
        }
    }
}